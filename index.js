const express = require('express')
const cors = require('cors')
const mysql = require('mysql')
const bodyParser = require('body-parser')
const app = express()
app.use(cors())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json({ limit: '10mb' }))

const credentials = {
	host: 'localhost',
	user: 'root',
	password: 'root',
	database: 'banco',
	port:'8889'
}

app.post('/api/login', (req, res) => {
	const { email, password } = req.body
	const values = [email, password]
	var connection = mysql.createConnection(credentials)
	connection.query("SELECT * FROM users WHERE email = ? AND password = ?", values, (err, result) => {
		if (err) {
			res.status(500).send(err)
		} else {
			if (result.length > 0) {
				res.status(200).send({
					'success': true,
					'id': result[0].id,
					'user': result[0].user,
					'email': result[0].email
				})
			} else {
				res.status(400).send({
					'success': false,
					'msg': 'Credenciales inválidas'
				})
			}
		}
	})
	connection.end()
})


app.listen(4000, () => console.log('Servidor activo'))